from constants import ALL_POSSIBLE_MOVES
from constants import COMBINATIONS_JSON_FILENAME
from constants import DATA_GRAPH_FILE_NAME
from constants import NUM_TO_ADD_FOR_A_DRAW
from constants import NUM_TO_ADD_FOR_A_LOSS
from constants import NUM_TO_ADD_FOR_A_WIN
from utilities import isSolved
from utilities import loadJson
from utilities import saveJson

import matplotlib.pyplot as plt
import os
import random


# randomly selects where to play
def pickWhereToPlay(possibleMoves):
    return random.choices(*zip(*possibleMoves.items()), k=1)[0]


def gameStateToString(gameState):
    return "".join(gameState)


# Function to handle the computers play with random generation based on the weights
# in the combinations json
# returns 0 if there are no possible moves (forfeit)
# returns 1 if a move was successful
def letComputerPlay(gameState, gameStatesEncounteredAndMovesMadeMap):
    combinations = loadJson(COMBINATIONS_JSON_FILENAME)
    possibleMoves = combinations[gameStateToString(gameState)]
    if all(possibleMoves[possibleMove] == 0 for possibleMove in possibleMoves):
        return 0
    move = pickWhereToPlay(possibleMoves)
    gameStatesEncounteredAndMovesMadeMap[gameStateToString(gameState)] = move
    print(f"\n\nComputer played {move}")
    gameState[ALL_POSSIBLE_MOVES.index(move)] = "1"
    return 1


def printGrid(gameState):
    symbols = [" ", "X", "O"]
    print("\n")
    for i in range(9):
        if i == 3 or i == 6:
            print("\n--+---+--")
        print(symbols[int(gameState[i])], end="")
        if i % 3 != 2:
            print(" | ", end="")


def letPlayerPlay(gameState):
    while True:
        print("\n\nUse this for help as to where to play")
        for i in range(9):
            if i == 3 or i == 6:
                print("\n---+----+---")
            print(ALL_POSSIBLE_MOVES[i], end="")
            if i % 3 != 2:
                print(" | ", end="")
        print("\nWhere do you want to play?")
        choice = input()
        if (
            choice not in ALL_POSSIBLE_MOVES
            or gameState[ALL_POSSIBLE_MOVES.index(choice)] != "0"
        ):
            print("That is an illegal move")
        else:
            break

    gameState[ALL_POSSIBLE_MOVES.index(choice)] = "2"


# Update the count for the specific moves made in the game that just concluded
# +3 for a win
# +1 for a draw
# -1 for a loss
def updateCombinationsJson(gameState, gameStatesEncounteredAndMovesMadeMap, change):
    combinations = loadJson(COMBINATIONS_JSON_FILENAME)
    for gameState in gameStatesEncounteredAndMovesMadeMap:
        gameStateAsString = gameStateToString(gameState)
        if gameStateAsString in combinations:
            moveMade = gameStatesEncounteredAndMovesMadeMap[gameStateAsString]
            combinations[gameStateAsString][moveMade] += change
            if combinations[gameStateAsString][moveMade] < 0:
                combinations[gameStateAsString][moveMade] = 0
    saveJson(combinations, COMBINATIONS_JSON_FILENAME)


def updateDataFile():
    fin = open("Data.txt", "r")
    numTimesPlayed = len(fin.readlines()) - 1
    fin.close()
    fout = open("Data.txt", "a")
    combinations = loadJson(COMBINATIONS_JSON_FILENAME)
    numInFirstBox = sum(combinations["000000000"].values())
    fout.write(str(numTimesPlayed) + "\t" + str(numInFirstBox) + "\n")
    fout.close()


# Plot a graph
# x-Axis - number of games played
# y-Axis - number of possible moves on a blank board
#
# Used to track MENACE's progress
# The higher the number of possible moves on a blank board, the better
def plotGraph():
    fin = open("Data.txt", "r")
    xValues = []
    yValues = []
    flag = True
    plt.xlabel("Number of games played")
    plt.ylabel("Number of possible moves in first position")
    for line in fin:
        line = line.strip().split()
        if flag:
            flag = False
        else:
            xValues.append(int(line[0]))
            yValues.append(int(line[1]))
    plt.plot(xValues, yValues)
    plt.xlim(xValues[0], xValues[-1])
    plt.ylim(0, max(yValues))
    plt.title("Data")
    plt.savefig(DATA_GRAPH_FILE_NAME)
    fin.close()


def playGame(gameState, gameStatesEncounteredAndMovesMadeMap):
    win = None
    for i in range(6):
        if letComputerPlay(gameState, gameStatesEncounteredAndMovesMadeMap) == 0:
            # Forfeit
            print("\nComputer resigns")
            win = -1
            break
        printGrid(gameState)
        if isSolved(gameState):
            print("\nComputer wins")
            win = 1
            break
        if "0" not in gameState:
            print("\nDraw")
            win = 0
            break
        letPlayerPlay(gameState)
        if isSolved(gameState):
            print("\nPlayer wins")
            win = -1
            break
        printGrid(gameState)

    return win


def main():
    gameState = ["0"] * 9
    gameStatesEncounteredAndMovesMadeMap = {}
    win = playGame(gameState, gameStatesEncounteredAndMovesMadeMap)

    if win == 1:  # Win for Computer
        updateCombinationsJson(
            gameState,
            gameStatesEncounteredAndMovesMadeMap,
            NUM_TO_ADD_FOR_A_WIN,
        )
    elif win == 0:  # Draw
        updateCombinationsJson(
            gameState,
            gameStatesEncounteredAndMovesMadeMap,
            NUM_TO_ADD_FOR_A_DRAW,
        )
    elif win == -1:  # Win for player
        updateCombinationsJson(
            gameState,
            gameStatesEncounteredAndMovesMadeMap,
            NUM_TO_ADD_FOR_A_LOSS,
        )

    # put into data file
    updateDataFile()

    # plot on graph
    plotGraph()


if __name__ == "__main__":
    main()
