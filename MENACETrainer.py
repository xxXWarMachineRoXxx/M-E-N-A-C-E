from copy import deepcopy
from utilities import isSolved

import MENACE
import random
import sys


def isInt(potentialNumber):
    try:
        int(potentialNumber)
    except:
        return False
    finally:
        return True


# Override for letPlayerPlay from MENACE
# If a particular move will win the game for the trainer,
# it goes ahead and plays that instead
# Else, randomly choose a playable slot and play there
def letPlayerPlayOverride(gameState):
    placesToPlay = [i for i in range(len(gameState)) if gameState[i] == "0"]

    for placeToPlay in placesToPlay:
        gameStateCopy = deepcopy(gameState)
        gameStateCopy[placeToPlay] = "2"
        if isSolved(gameStateCopy):
            gameState[placeToPlay] = "2"
            return
    else:
        gameState[random.choice(placesToPlay)] = "2"


def main():
    MENACE.letPlayerPlay = letPlayerPlayOverride
    if len(sys.argv) != 2 or not isInt(sys.argv[1]):
        print("Error: Please input a number", file=sys.stderr)
        sys.exit(0)

    for i in range(int(sys.argv[1])):
        MENACE.main()


if __name__ == "__main__":
    main()
