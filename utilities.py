import json


def loadJson(filename):
    fin = open(filename, "r")
    data = json.load(fin)
    fin.close()

    return data


def saveJson(data, filename):
    fout = open(filename, "w")
    json.dump(data, fout, sort_keys=True, indent=4)
    fout.close()


# checking for three of an 'x' or an 'o' in rows
def checkRows(gameState):
    for i in range(3):
        if (
            gameState[3 * i] == gameState[3 * i + 1]
            and gameState[3 * i] == gameState[3 * i + 2]
            and gameState[3 * i] != "0"
        ):
            return True
    return False


# checking for three of an 'x' or an 'o' in columns
def checkCols(gameState):
    for i in range(3):
        if (
            gameState[i] == gameState[i + 3]
            and gameState[i] == gameState[i + 6]
            and gameState[i] != "0"
        ):
            return True
    return False


# checking for three of an 'x' or an 'o' in diagonals
def checkDiags(gameState):
    # Check Principal Diagonal
    if (
        gameState[0] == gameState[4]
        and gameState[0] == gameState[8]
        and gameState[0] != "0"
    ):
        return True

    # Check Non-Principal Diagonal
    if (
        gameState[2] == gameState[4]
        and gameState[2] == gameState[6]
        and gameState[2] != "0"
    ):
        return True
    return False


# check if a grid is solved
def isSolved(gameState):
    return checkRows(gameState) or checkCols(gameState) or checkDiags(gameState)
